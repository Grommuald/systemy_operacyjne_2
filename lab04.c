// SO2 IS1 222A LAB04
// Aleksander Grobicki-Madej
// agrobicki@wi.zut.edu.pl

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int quit_loop = 0;
int has_sigtstp_occurred = 0;

char* validate_string(char*, int*);
void sigint_handler(int);
void sigtstp_handler(int);

int main(int argc, char** argv) {
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGTSTP);
    sigprocmask(SIG_BLOCK, &sigset, NULL);
    
    struct sigaction act;
    act.sa_handler = &sigint_handler;
	
    if (argc > 1) {
        int allocation_flag = 0;
        char* primary_string = validate_string(argv[argc-1], &allocation_flag);
        int primary_string_len = strlen(primary_string); 

        char* env[] = { NULL };
        char** input_args = (char**) malloc((argc+2)*sizeof(char*));

        int status = 0;

        for (int i = 0; i < argc-1; ++i) {
            input_args[i] = strdup(argv[i]);
        }
        input_args[argc-1] = strdup(primary_string);
                
        pid_t child1 = -1;
        pid_t child2 = -1;
        
        if (primary_string_len >= 1) { 
            sigaction(SIGINT, &act, NULL);
            
            child1 = fork();
            if (child1 == 0) {
                setpgid(child1, 0);
                sigaction(SIGINT, &act, NULL);

                input_args[argc] = strndup(&primary_string[0], primary_string_len/2);
                input_args[argc+1] = NULL;
                execve(input_args[0], input_args, env);
            } else { 
                child2 = fork();
                if (child2 == 0) {
                    setpgid(child2, 0);
                    sigaction(SIGINT, &act, NULL);

                    input_args[argc] = strndup(&primary_string[primary_string_len/2], primary_string_len/2);
                    input_args[argc+1] = NULL;
                    execve(input_args[0], input_args, env);
                }
            }
        }
        while (!quit_loop)
            ;
        
        kill(child1, SIGINT);
        waitpid(child1, &status, 0);
        
        kill(child2, SIGINT);
        waitpid(child2, &status, 0);
        
        printf("pid:%d pgid:%d ", getpid(), getpgid(0));
        for (int i = 1; i < argc; ++i) {
            printf("%s ", input_args[i]);
        }
        puts("");
        for (int i = 0; i < argc; ++i)
            if (input_args[i])
                free(input_args[i]);

        if (allocation_flag && primary_string) {
            free(primary_string);
        }
	} else {
	    puts("Please, provide at least one argument.");    	
	}
    act.sa_handler = &sigtstp_handler;
    sigaction(SIGTSTP, &act, NULL);
    sigprocmask(SIG_UNBLOCK, &sigset, NULL);

	return 0;
}

char* validate_string(char* string, int* allocation_flag) { 
    int string_length = strlen(string);

    if ((string_length != 0) &&
            ((string_length & (string_length-1)) == 0)) {
        return string;
    } else {
        *allocation_flag = 1;

        int tmp_length = string_length;
        int i = 0;
        while (tmp_length >>= 1) {
            ++i;
        } 
        int new_length = 1;
        for (int j = 0; j < i+1; ++j) {
            new_length *= 2;
        }
        
        int floor = 0;
        if (string_length < ((new_length + (new_length / 2)) / 2)) {
            new_length /= 2;
            floor = 1;
        }
        char* new_primary = (char*) malloc((new_length+1)*sizeof(char));
        if (floor) {
            strncpy(new_primary, string, new_length);
        } else { 
            strncpy(new_primary, string, string_length); 
            i = string_length;
            while (i < new_length) {
                new_primary[i] = 'a'+(i-string_length);
                ++i;
            }
        }
        new_primary[new_length] = '\0';
        return new_primary;
    }
}
void sigint_handler(int s) {
    quit_loop = 1;    
}
void sigtstp_handler(int s) {
    puts("SGTSTP has been cancelled."); 
}
