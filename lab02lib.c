// SO2 IS1 222A LAB02
// Aleksander Grobicki-Madej
// agrobicki@wi.zut.edu.pl

#include "lib.h"
#include <string.h>
#include <utmpx.h>
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <pwd.h>

int show_pid(const char* username) {
	struct passwd* pwuid = getpwnam(username);
	if (!pwuid) {
		puts("\nCould not get passwd adress.");
		return -1;
	}
	printf("%d %s\n", pwuid->pw_uid, pwuid->pw_name);
	return 0;
}

int show_groups(const char* username) {	
	struct passwd* pwuid = getpwnam(username);	
	if (!pwuid) {
		puts("\nCould not get passwd adress.");
		return -1;
	}
	int ngroups = 32;
	int ngroups_max = ngroups;

	gid_t* groups = (gid_t*) malloc(ngroups * sizeof(gid_t));
	if (!groups) {
		puts("\nCould not allocate the memory.");
		return -1;
	}
	if (getgrouplist(pwuid->pw_name, pwuid->pw_gid,
		groups, &ngroups) == -1) {
		puts("\nCould not get the grouplist."); 
		return -1;
	} else {
		if (ngroups != ngroups_max) {
			if (!(groups = (gid_t*) realloc(groups, ngroups * sizeof(gid_t)))) {
				puts("\nCould not reallocate the memory.");
			}
			getgrouplist(pwuid->pw_name, pwuid->pw_gid, groups, &ngroups);
		}
		printf("%s [", pwuid->pw_name);
		for (int i = 0; i < ngroups; ++i) {
			struct group* gr = getgrgid(groups[i]);
			if (gr)
				if (i == ngroups - 1)
					printf("%s]\n", gr->gr_name);
				else
					printf("%s ", gr->gr_name);
		}
	}
	if (groups)
		free(groups);
	return 0;
}
