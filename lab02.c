// SO2 IS1 222A LAB02
// Aleksander Grobicki-Madej
// agrobicki@wi.zut.edu.pl

#include <dlfcn.h>
#include <unistd.h>
#include <utmpx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void show_users_only(char**, int);

int main(int argc, char** argv) {
	void* handle = dlopen("./lib.so.0.3", RTLD_LAZY);
    
    struct utmpx* current_utmpx = NULL;
    int number_of_users = 0;
    
    setutxent();
    while (current_utmpx = getutxent()) {
        if (current_utmpx->ut_type == 7)
            ++number_of_users;
    }
    endutxent();
    char** users = (char**) malloc(number_of_users*sizeof(char*));
    int i = 0;
    setutxent();
    while (current_utmpx = getutxent()) {
        if (current_utmpx->ut_type == 7) {
            users[i++] = strdup(current_utmpx->ut_user);
        }
    }
    endutxent();
       
    if (!handle) {
       puts("Could not find library file. Switching to default mode...");
       show_users_only(users, number_of_users);        
    } else {
		int (*show_pid)(const char*) = dlsym(handle, "show_pid");
		int (*show_groups)(const char*) = dlsym(handle, "show_groups");
		
		char opt = 0;
		char pids_flag = 0;
		char groups_flag = 0;
		char err_flag = 0;

		while ((opt = getopt(argc, argv, "bz")) != -1) {
			switch (opt) {
				case 'b':
				pids_flag = 1;
				break;

				case 'z':
				groups_flag = 1;
				break;
				
				case '?':
				err_flag = 1;
				break;
			}
		}
        if (!err_flag) {
			if (pids_flag) {
				if (show_pid) {
					for (i = 0; i < number_of_users; ++i) {
						show_pid(users[i]);
					}
				} else {				
					puts("Could not find pid function.\nSwitching to default mode...");
					show_users_only(users, number_of_users);
				}
			} else if (groups_flag) {
				if (show_groups) {
					for (i = 0; i< number_of_users; ++i) {
						show_groups(users[i]);
					}
				} else {
					puts("Could not find group function.\nSwitching to default mode...");
					show_users_only(users, number_of_users);
				}
			} else
				show_users_only(users, number_of_users);
		
            for (int i = 0; i < number_of_users; ++i)
                if (users[i])
                    free(users[i]);
        } else {
	        show_users_only(users, number_of_users);	
            puts("Wrong options were chosen. Please select one of these options: -b -z or don't select any.");
        } 
        dlclose(handle);
	}
	return 0;
}

void show_users_only(char** users, int number_of_users) {	
	for (int i = 0; i < number_of_users; ++i) {
		printf("%s\n", users[i]);
	}

}
