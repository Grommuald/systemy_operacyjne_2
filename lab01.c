// SO2 IS1 222A LAB01
// Aleksander Grobicki-Madej
// agrobicki@wi.zut.edu.pl

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <utmpx.h>
#include <grp.h>
#include <pwd.h>

int show_groups(struct utmpx*);
int show_pids(struct utmpx*);

int main(int argc, char** argv) {
	const char* err = "Wrong options were chosen. Avilable options: -b -z"; 
	char opt = 0;	

	char pids_flag = 0;
	char groups_flag = 0;
	char err_flag = 0;

	while ((opt = getopt(argc, argv, "bz")) != -1) {
		switch (opt) {
			case 'b':
			pids_flag = 1;
			break;
			
			case 'z':
			groups_flag = 1;
			break;
			
			case '?':
			err_flag = 1;
			break;
		}
	}
	if (!err_flag) {	
		struct utmpx* current_utmpx = NULL;
		setutxent();	
		while (current_utmpx = getutxent()) {
			if (current_utmpx->ut_type == 7) {
				if (pids_flag)
					show_pids(current_utmpx);
				printf("%s", current_utmpx->ut_user);
				if (groups_flag)
					show_groups(current_utmpx);
				puts("");
			}
		}
		endutxent();
	} else {
		puts(err);
	}
	return 0;
}

int show_pids(struct utmpx* utmpx_struct) {
	struct passwd* pwuid = getpwnam(utmpx_struct->ut_user);
	if (!pwuid) {
		puts("\nCould not get passwd address.");
		return -1;
	}
	printf("%d ", pwuid->pw_uid);
	return 0;
}
int show_groups(struct utmpx* utmpx_struct) {
	struct passwd* pwuid = getpwnam(utmpx_struct->ut_user);	
	if (!pwuid) {
		puts("\nCould not get passwd address.");
		return -1;
	}
	int ngroups = 32;
	int ngroups_max = ngroups;

	gid_t* groups = (gid_t*) malloc(ngroups * sizeof(gid_t));
	if (!groups) {
		puts("\nCould not allocate the memory.");
		return -1;
	}
	if (getgrouplist(utmpx_struct->ut_user, pwuid->pw_gid,
		groups, &ngroups) == -1) {
		puts("\nCould not get the grouplist");	
	} else {
		if (ngroups != ngroups_max) {
			if (!(groups = (gid_t*) realloc(groups, ngroups * sizeof(gid_t)))) {
				puts("\nCould not reallocate the memory.");
				 return -1;
			}
			getgrouplist(utmpx_struct->ut_user, pwuid->pw_gid, groups, &ngroups);
		}

		printf(" [");
		for (int i = 0; i < ngroups; ++i) {
			struct group* gr = getgrgid(groups[i]);
			if (gr) {
				if (i == ngroups - 1) {
					printf("%s]", gr->gr_name);
				} else 
					printf("%s, ", gr->gr_name);
			}
		}
	}
	if (groups)
		free(groups);
	return 0; 	
}
