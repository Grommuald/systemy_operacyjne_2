// SO2 IS1 222A LAB05
// Aleksander Grobicki-Madej
// agrobicki@wi.zut.edu.pl

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>

char* get_process_name(const char*);

int main(int argc, char** argv) {
    if (argc == 1) {
        puts("No arguments were provided.");        
        return 1;
    }   
    const char* PROC_DIRECTORY_PATH = "/proc";
    
    struct dirent* file = NULL;
    DIR* proc_path = NULL;    
    
    int i = 1;
    char at_least_one_pid_printed = 0;
    char killall = 0;

    if (strcmp(argv[0], "./killall") == 0) {
        killall = 1;
    }

    while (argv[i]) {
        int isNumberYet = 0;
        proc_path = opendir(PROC_DIRECTORY_PATH);
        while ((file = readdir(proc_path))) {
            if (!isNumberYet && strcmp(file->d_name, "1") == 0) {
                isNumberYet = 1;
            }     
            if (isNumberYet) {
                char* process_name = get_process_name(file->d_name);
                if (process_name) {
                    if (strcmp(process_name, argv[i]) == 0) {
                        if (killall) {
                            if (kill(atoi(file->d_name), SIGINT) != 0)
                                perror(NULL);
                        } else {
                            printf("%s ", file->d_name);
                            at_least_one_pid_printed = 1;
                        }
                    }
                    free(process_name);
                }
            }
        }
        closedir(proc_path);
        i++;
    }
    if (at_least_one_pid_printed)
        puts("");
    return 0;
}

char* get_process_name(const char* pid) {
    char* name = (char*) calloc(64, sizeof(char));
    if (name) {
        sprintf(name, "/proc/%s/comm", pid);
        FILE* f = fopen(name, "r");
        if (f) { 
            free(name);
            const size_t SIZE = 128;
            name = (char*) calloc(SIZE, sizeof(char));
            
            size_t i = 0;
            char c;
            while (i < SIZE && (c = fgetc(f)) != EOF && c != '\n') {
                name[i++] = (char)c;
            }
            fclose(f);
        }
    }
    return name;
}
